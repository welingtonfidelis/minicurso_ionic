import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NewUserPage } from '../new-user/new-user.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  listaUsuarios = []

  constructor(private modalController: ModalController,
    private toastController: ToastController,
    private storage: Storage) {
    // this.listaUsuarios = [
    //   { nome: 'teste 1', telefone: 999999 },
    //   { nome: 'teste 2', telefone: 999999 },
    //   { nome: 'teste 3', telefone: 999999 },
    // ]

    this.atualizaLista()
  }

  ngOnInit() {
  }

  // async novoUsuario() {
  //   const modal = await this.modalController.create({
  //     component: NewUserPage
  //   })

  //   modal.onDidDismiss()
  //     .then((retorno) => {
  //       if (retorno.data) this.listaUsuarios.push(retorno.data)
  //     })
  //   return await modal.present()
  // }

  async novoUsuario() {
    const modal = await this.modalController.create({
      component: NewUserPage
    })

    modal.onDidDismiss()
      .then((retorno) => {
        this.atualizaLista()
      })
    return await modal.present()
  }

  // async editaUsuario(usuario, index){
  //   const modal = await this.modalController.create({
  //     component: NewUserPage,
  //     componentProps: {usuario}
  //   })

  //   modal.onDidDismiss()
  //     .then((retorno) => {
  //       if (retorno.data){
  //         this.listaUsuarios.splice(index, 1)

  //         this.listaUsuarios.push(retorno.data)
  //       } 
  //     })
  //   return await modal.present()
  // }

  async editaUsuario(usuario, index){
    const modal = await this.modalController.create({
      component: NewUserPage,
      componentProps: {usuario}
    })

    modal.onDidDismiss()
      .then((retorno) => {
        if (retorno.data){
          this.listaUsuarios.splice(index, 1)

          this.listaUsuarios.push(retorno.data)

          this.storage.set('listaUsuarios', this.listaUsuarios)
        } 
      })
    return await modal.present()
  }

  // deletaUsuario(index){
  //   this.listaUsuarios.splice(index, 1)
  //   this.presentToast('Excluido com sucesso')
  // }

  deletaUsuario(index){
    this.listaUsuarios.splice(index, 1)
    this.storage.set('listaUsuarios', this.listaUsuarios)
    this.presentToast('Excluido com sucesso')
  }

  atualizaLista(){
    this.storage.get('listaUsuarios')
    .then((data)=>{
      this.listaUsuarios = data
    })
  }

  async presentToast(msg = 'nada') {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  alerta() {
    alert('Olá')
  }
}

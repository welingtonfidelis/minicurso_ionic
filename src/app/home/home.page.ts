import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  latitude = 1
  longitude  = 1
  watch

  constructor(
    private geolocation: Geolocation,
    private toastController: ToastController) {
    
  }

  iniciaLoc(){
    this.watch = this.geolocation.watchPosition()
    .subscribe((position)=>{
      this.latitude = position.coords.latitude
      this.longitude = position.coords.longitude

      console.log('mudou');
      
    })

    this.presentToast('Localizando você')
  }

  paraLoc(){
    this.watch.unsubscribe();

    this.presentToast('Parei de localizar você')
  }

  async presentToast(msg = 'nada') {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}

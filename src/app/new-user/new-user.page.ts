import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.page.html',
  styleUrls: ['./new-user.page.scss'],
})
export class NewUserPage implements OnInit {
  nome
  telefone
  atualizacao = false

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private toastController: ToastController,
    private storage: Storage) {

      if(this.navParams.get('usuario')){
        const usuario = this.navParams.get('usuario')
        this.nome = usuario.nome
        this.telefone = usuario.telefone
        this.atualizacao = true
      }
     }

  ngOnInit() {
  }

  // salvar(){
  //   const retorno = {
  //     nome: this.nome,
  //     telefone: this.telefone
  //   }

  //   this.presentToast('Salvo com sucesso')

  //   this.modalController.dismiss(retorno)
  // }

  salvar(){
    const obj = {
      nome: this.nome,
      telefone: this.telefone
    }

    this.presentToast('Salvo com sucesso')

    if(!this.atualizacao){
      let retorno = []
      this.storage.get('listaUsuarios').then((data)=>{
        if(data) retorno = data
  
        retorno.push(obj)
  
        this.storage.set('listaUsuarios', retorno)
      })
    }

    this.modalController.dismiss(obj)
  }

  sair(){
    this.modalController.dismiss()
  }

  async presentToast(msg = 'nada') {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
